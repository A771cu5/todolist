import Ember from 'ember';

export default Ember.Route.extend({
  
  model() {
    return this.store.createRecord('todo', {});
  },

  onSuccessfulSave() {
    this.transitionTo('todos');
  },

  actions: {
    cancel() {
      this.transitionTo('todos');
    },

    save() {
      let newTodo = this.get('currentModel');
      let self = this;
      newTodo.save()
        .then(() => { self.onSuccessfulSave(); })
    },

    willTransition() {
      let newTodo = this.get('currentModel');
      if (newTodo.get('isNew') && !newTodo.get('isSaving')) {
        newTodo.deleteRecord();
      }
    }
  }
});
