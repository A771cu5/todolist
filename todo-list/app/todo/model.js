import DS from 'ember-data';

export default DS.Model.extend({
  description: DS.attr('string'),
  complete: DS.attr('boolean', { defaultValue: false }),
  createdAt: DS.attr('date', { defaultValue: function() { return new Date(); } })
});
