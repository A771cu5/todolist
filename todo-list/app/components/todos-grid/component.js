import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'table',
  classNames: [ 'table', 'table-striped' ],

  syncedTodos: Ember.computed.filterBy('todos', 'isNew', false),

  incompleteTodos: Ember.computed.filterBy('syncedTodos', 'complete', false),

  sortProperties: [ 'createdAt:desc'],
  sortedTodos: Ember.computed.sort('incompleteTodos', 'sortProperties')

});
