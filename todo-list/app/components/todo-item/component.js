import Ember from 'ember';

export default Ember.Component.extend({
  tagName: 'tr',

  actions: {
    finish() {
      let myTodo = this.get('todo');
      myTodo.set('complete', true);
      myTodo.save();
    }
  }
});
