class Todo < ApplicationRecord

  scope :incomplete, -> { where complete: false }

end
