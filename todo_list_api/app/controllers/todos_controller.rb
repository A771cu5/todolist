class TodosController < ApplicationController

  def index
    render json: Todo.incomplete, status: :ok, each_serializer: TodoSerializer
  end

  def show
    render json: requested_todo, status: :ok, serializer: TodoSerializer
  end

  def create
    if new_todo.save
      render json: new_todo, status: :created, serializer: TodoSerializer
    else
      render json: new_todo, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end

  def update
    if requested_todo.update(update_params)
      render json: requested_todo, status: :ok, serializer: TodoSerializer
    else
      render json: requested_todo, status: :unprocessable_entity, serializer: ActiveModel::Serializer::ErrorSerializer
    end
  end

  private

  def requested_todo
    @todo ||= Todo.find params[:id]
  end

  def new_todo
    @todo ||= Todo.new create_params
  end

  def create_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:description])
  end

  def update_params
    ActiveModelSerializers::Deserialization.jsonapi_parse(params, only: [:complete])
  end

end
