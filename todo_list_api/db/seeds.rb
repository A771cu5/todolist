# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

50.times do |i|
  Todo.create(
    description: FFaker::Lorem.words(3).join(' ').titleize,
    complete: [true, false].sample,
    created_at: (DateTime.now - i.minutes)
  )
end
