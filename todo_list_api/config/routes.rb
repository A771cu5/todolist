Rails.application.routes.draw do

  scope constraints: { format: :json } do
    resources :todos, only: [ :index, :show, :create, :update ]
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
